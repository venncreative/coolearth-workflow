# Cool Earth Workflow Proposal

This is a project structure proposal for Cool Earth. Everything is up for discussion!

## Principles

__Simple setup__

- Should be able to get setup with a few commands
- Instructions should be clear and provided in the repository (e.g as a README)

__Organised structure__

- Clean working directory to make it easy to make changes
- Seperation, as best as possible, of repository code and third party

__Portability__

- Cool Earth site has been moved to various different servers, this should be a simple task with the way the project is structured and managed
- Configuration is in one place and easy to change, whether thats in a `/config` folder outside of the `/public` directory or in one file.

### Vagrant

- Setup local environment through Vagrant
- Box provisioned to match servers as best as possible
- Contains Apache (nginx), PHP, MySQL - whatever is running on servers

### WordPress

__WordPress Core__

- Core download can be managed through wp-cli
- Core version specified in wp-cli.yml config file
- Core not commited to repository

__Contents Direcotry__

- Contents directory is seperate from core installtion
- `/uploads` directory is not committed to repository
- The Cool Earth __theme__ and __all plugins__ are committed to the repo

_Unfortunately the way WordPress manages plugins its difficult to manage what plugins are installed and their version without including them in the repo._


### Ideal setup

##### 1. Clone the repository

Everything we need to setup a local environment should exist in a single repository.

##### 2. Download WordPress

Through wp-cli `wp core download`, it will grab the correct version specified in the `wp-cli.yml` config file.

Alternatively, can be downloaded and unzipped. Just needs to be placed in `public\wp` and be the version specified in the `wp-cli.yml` file.

##### 3. Run Vagrant

Run `vagrant up` to create a virtual server that symlinks the working directory. Can be accessed through the browser at `localhost:8080` or `coolearth.local`, or whatever, just need to specify any additional setup to get it working.

##### 4. Setup Database

Whether this can be done within Vagrant provisioning, or is simply importing a couple of mysql dump files. Need a database with users for logging into the admin section as well as the Cool Earth content. Dummy data for CiviCRM (contributions, campaigns etc) for development is needed.


